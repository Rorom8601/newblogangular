import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Article, Comments } from './entities';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private http: HttpClient) { }

  getAllSimpleArticles(){
    return this.http.get<Article[]>(environment.apiUrl+'/api/article');
  }
  getArticleById(id:number){
    return this.http.get<Article>(environment.apiUrl+'/api/article/only/'+id);
  }
  getArticleByIdWithComments(id:number){
    return this.http.get<Article>(environment.apiUrl+'/api/article/'+id);
  }
  deleteArticleById(id:number){
    return this.http.delete(environment.apiUrl+'/api/article/'+id);
  }
  deleteAllComments(id:number){
    return this.http.delete(environment.apiUrl+'/api/article/comment/'+id);
  }
  addArticle(article:Article,categoryId:number){
    return this.http.post<Article>(environment.apiUrl+'/api/article/'+categoryId, article);
  }
  saveCommentByArticleId(comments:Comments, id:number){
    return this.http.post(environment.apiUrl+'/api/article/comment/'+id,comments);
  }
  updateArticle(article:Article, id:number){
    return this.http.patch<Article>(environment.apiUrl+'/api/article/'+id, article);
  }
}
