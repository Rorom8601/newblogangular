import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/internal/operators/switchMap';
import { ArticleService } from '../article.service';
import { Article, Comments } from '../entities';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
@Input()
article?:Article;
comments:Comments={
  id:0,
  label:''
}
modifiedArticle:Article={
  id:0,
  title:'',
  content:'',
  author:'',
  date:''
}
displayForm:boolean=false;

  constructor(private route:ActivatedRoute, private artService: ArticleService, private router:Router) { }

  showEditForm(article:Article){
    this.displayForm=true;
    this.modifiedArticle=Object.assign({},article);
   
    
  }

  update(article:Article){
    this.displayForm=false;
    this.artService.updateArticle(article,this.article!.id).subscribe();
    this.article={...this.modifiedArticle};
  }

  getArticleById(id:number){
    this.artService.getArticleById(id).subscribe(data=>this.article=data)
  }
  deleteArticle(){
    this.artService.deleteAllComments(this.article!.id).subscribe();
    this.artService.deleteArticleById(this.article!.id).subscribe();
    this.router.navigateByUrl('');
  }
  saveComment(){
    this.artService.saveCommentByArticleId(this.comments!,this.article!.id).subscribe();
    this.article?.listComments?.push({...this.comments});
    this.clearField();
  }
  clearField(){
    this.comments.label='';
  }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.artService.getArticleByIdWithComments(params['id']))
    )
    .subscribe(data => this.article=data);
  }

}
