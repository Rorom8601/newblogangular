import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Comments } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private http:HttpClient) { }

  getAll(){
    return this.http.get<Comments[]>(environment.apiUrl+"/api/comments");
  }
  getCommentById(id:number){
    return this.http.get<Comments>(environment.apiUrl+"/api/comments/"+id);
  }
  deleteComment(id:number){
    return this.http.delete(environment.apiUrl+"/api/comments/"+id);
  }
  addComments(comments:Comments){
    return this.http.post<Comments>(environment.apiUrl+"/api/comments",comments);
  }
}
