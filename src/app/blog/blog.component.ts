import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../article.service';
import { CategoryService } from '../category.service';
import { CommentsService } from '../comments.service';
import { Article, Category, Comments } from '../entities';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
//Articles
  selectedArticle?: Article;
  articleList?: Article[];
  articleToAdd:Article={
    id:0,
    title:'',
    content:'',
    author:'',
    date:'2022-05-05'
  };
//Comments
  commentsList?:Comments[];
  selectedComment?:Comments;
  commentToAdd:Comments={
    id:0,
    label:''
  };
//Categories
listCategories?:Category[];
categoryToAdd:Category={
  id:0,
  label:'',
  listArticle:[]
}


constructor(private artService: ArticleService, private comService: CommentsService, private catService: CategoryService) { }


  //Articles
  addArticle(){
    this.artService.addArticle(this.articleToAdd!,5).subscribe();
  }

  getAllArticles() {
    this.artService.getAllSimpleArticles().subscribe(data => this.articleList = data);
  }
  getArticleById(id: number) {
    id = 10;
    this.artService.getArticleById(id).subscribe(data => this.selectedArticle = data);
  }
  deleteArticleById(id: number) {
    this.artService.deleteArticleById(id).subscribe();
    this.deleteFromList(id);

  }

  deleteFromList(id:number){
    this.articleList!.forEach((article, index)=>{
      if(article.id==id){
        this.articleList!.splice(index,1);
      }
    });
  }


  //Comments
  getCommentList(){
    this.comService.getAll().subscribe(data=>this.commentsList=data);
  }
  getCommentById(){
    let id=15;
    this.comService.getCommentById(id).subscribe(data=>this.selectedComment=data);
  }
  deleteComment(id:number){
    this.comService.deleteComment(id).subscribe();
    this.deleteCommentFromList(id);
  }
  deleteCommentFromList(id:number){
    this.commentsList?.forEach((comment,index)=>{
        if(comment.id==id){
          this.commentsList?.splice(index,1);
        }
    });
  }

  addComment(){
    this.comService.addComments(this.commentToAdd).subscribe();
  }
//Category
getCategories(){
  this.catService.getAllCategories().subscribe(data=>this.listCategories=data);
}
getCategoriesWithArticles(){
  this.catService.getAllCategoriesWithArticles().subscribe(data=>this.listCategories=data);
}
deleteCategory(id:number){
  this.catService.deleteCategory(id).subscribe();
}
addCategory(){
  this.catService.addCategory(this.categoryToAdd).subscribe();
}


  ngOnInit(): void {
  
  }
}





