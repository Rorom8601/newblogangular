import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { CategoryService } from '../category.service';
import { Article, Category } from '../entities';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css']
})
export class AddArticleComponent implements OnInit {

  constructor(private router: Router, private artService: ArticleService, private catService: CategoryService) { }
  article: Article = {
    id: 0,
    title: '',
    content: '',
    author: '',
    date: '',
  }
  categoryList!: Category[];
  categoryId?: number;

  addArticle() {
    let now = new Date();
    this.article!.date = now.toISOString();
    if (this.categoryId == undefined) {
      alert("Merci de saisir une catégorie !");
    }
    else if ((this.article.title.length == 0) || (this.article.content.length == 0) || (this.article.author.length == 0)) {
      alert("Il semble qu'un champ au moins soit vide")
    }
    else {
      this.artService.addArticle(this.article, this.categoryId).subscribe();
      //redirect to home
      this.router.navigateByUrl('');
    }


  }
  getCategories() {
    this.catService.getAllCategoriesWithArticles().subscribe(data => {
      this.categoryList = data
      console.log(this.categoryList)
    });
  }

  ngOnInit(): void {
    this.getCategories();
  }

}
