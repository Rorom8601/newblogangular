import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../article.service';
import { CategoryService } from '../category.service';
import { Article, Category } from '../entities';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  articleList: Article[] = [];
  categoryList!: Category[];
  listByCategory!: Article[];
  displayList!:Article[];
 

  constructor(private artService: ArticleService, private catService: CategoryService) { }

  getCategories() {
    this.catService.getAllCategoriesWithArticles().subscribe(data => this.categoryList = data);
  }
  
  getarticleByCategory() {
    if(this.listByCategory==null){
      this.displayList=this.articleList;
    }
    else{ 
      this.displayList =this.listByCategory; 
    }
  }

  getArticles() {
    this.artService.getAllSimpleArticles().subscribe(data => {
      this.articleList = data;
      this.displayList=data;
    });
    
  }

  ngOnInit(): void {
    this.getArticles();
    this.getCategories();
    
  }
}
