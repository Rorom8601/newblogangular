export interface Article{
  id: number,
  title:string,
  content:string,
  author:string,
  date:string,
  listComments?:Comments[]
}
export interface Comments{
  id:number,
  label:string
}
export interface Category{
  id:number,
  label:string,
  listArticle:Article[]
}