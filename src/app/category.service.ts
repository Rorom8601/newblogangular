import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Category } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http:HttpClient) { }

  getAllCategories(){
    return this.http.get<Category[]>(environment.apiUrl+"/api/category");
  }
  getAllCategoriesWithArticles(){
    return this.http.get<Category[]>(environment.apiUrl+"/api/categoryAll");
  }
  deleteCategory(id:number){
    return this.http.delete(environment.apiUrl+"/api/category/"+id);
  }
  addCategory(category:Category){
    return this.http.post<Category>(environment.apiUrl+"/api/category",category);
  }
}
