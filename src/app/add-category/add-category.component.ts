import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from '../category.service';
import { Category } from '../entities';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {

  constructor(private service: CategoryService, private route: Router) { }

  category: Category = {
    id: 0,
    label: "",
    listArticle: []
  }
  addNewCategory() {
    if (this.category.label.length == 0) {
      alert("Entrez un titre à la catégorie !")
    }
    else {
      this.service.addCategory(this.category).subscribe();
      this.route.navigateByUrl('');
    }
  }
  ngOnInit(): void {
  }

}
