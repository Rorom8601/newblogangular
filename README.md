# WireFrame

Comme indiqué dans l'énoncé, j'ai réalisé mes maquettes "mobile first", la page d'accueil présente la liste des articles présents avec la possibilité de faire une recherche par catégorie.  
Les articles sont présentés via des cards bootstrap indiquant le titre, l'auteur et la date de parution, un bouton mène vers le contenu de l'article.  
Le menu header permet la navigation vers les différentes pages et est sommairement mis en forme via bootrap.  
La page du contenu présente l'article plus des boutons permettant de supprimer ou de modifier l'article, dans ce dernier cas, un formulaire apparaît avec les données actuelles de l'article, l'utilisateur a alors la possibilité d'en modifier les valeurs. La validation de la suppression de l'article redirige l'utilisateur vers la page principale.  
Le site possède une page "Ajouter un article", elle contient un formulaire dont une dropdownList permettant (c'est d'ailleurs obligatoire) d'attribuer une catégorie à l'article ajouté. Le site contient aussi une page permettant d'ajouter une catégorie, la validation des différents ajouts redirige vers la page d'accueil.  
Une version pdf des maquettes est présente à la racine de ce site.  
Je joins aussi le lien vers la page figma contenant les wireframes:  
[Lien](https://www.figma.com/file/n8oiG2mVjSaezEoUAwuylB/blognode-id=0%3A1)  


# Base de données

Ma BDD contient 3 tables (article, category et comments), une clé étrangère (category_id) est présente est présente dans "article", une autre (article_id) est présente dans "comments" de manière à ce qu'un article puisse contenir plusieurs commentaires et que chaque article possède une catégorie.  
Le script d'execution de la BDD est présent dans le dossier ressource du projet back. Je mets, pour plus de facilité le lien vers ce script:  
[Lien](https://gitlab.com/Rorom8601/blogproject/-/blob/main/src/main/resources/database.sql)

# SringBoot

__Voici le lien GitLab vers le projet Springboot__:  
[lien](https://gitlab.com/Rorom8601/blogproject)  
Pour mes entités, j'ai utilisé la validation sur les propriètés que j'estimais essentielles, j'ai également imposé à la date de création de ne pas être dans le futur (peut être redondant car j'impose la date du jour à la création de l'article).  
Coté Repository, j'ai créé trop de méthodes, de nombreuses ne me servent pas comme "deleteAllCommentsFromArticle", je n'avais pas une idée très précise des fonctionnalités dont j'aurai besoin, j'ai prévu "trop large", si c'était à refaire, je commencerais mon projet Angular, puis, en fonction des fonctionnalités, j'inclurais une fonction prévue à cet effet. Ou j'aurais pu définir plus précisement les fonctionnalités de mon site...  
Idem pour les controlleurs.

# Angular

Dans le composant principal (app-component), j’ai écrit les header et footer,  
J’ai écrit un composant nommé « blog », je ne l’utilise pas dans la version finale, il m’a juste servi, au lancement du projet à faire tous les tests de mes requêtes sur l’API REST sur le port 8080.  
Mon composant d’accueil est le « homepageComponent », au chargement, dans le « ngOnInit, je récupère la liste des articles et des catégories via le composant service idoine injecté dans le constructeur. Je me sers des catégories pour peupler la dropdownList et des articles pour les afficher dans des cards.  
Pour filtrer les articles affichés dynamiquement en fonction de la catégorie sélectionnée, j’utilise le « category service » qui renvoie la liste des catégories avec leurs articles respectifs. Cette liste de catégories déclarée en TS est nommée « ListByCategory », elle est bindée avec la dropdown via un « ngModel ». Ensuite, j’assigne la liste filtrée (ou non) à la propriété « displayList ». J’utilise une boucle « ngFor » pour itérer sur cette dernière et ainsi afficher les card « articles ».  
J’utilise le « routerLink » d’angular pour, sur le click du bouton « lire l’article », rediriger l’utilisateur vers la page de consultation de l’article grâce à une route paramétrée déclarée dans le « app-routing module ».  
La page de consultation permet d’ajouter des commentaires. Ils sont visualisables dès validation grâce au passage par valeur (versus référence) grâce à cette mystérieuse instruction : `this.article?.listComments?.push({...this.comments});  `

Rien de particulier d’autre à noter dans le reste du projet si ce n’est que j’ai utilisé des media queries pour adapter mes textareas à l’affichage pour mobile dans les composants « article component » sur le click du bouton « Modifier" ainsi dans la page d’ajout d’articles.